const jwt = require("jsonwebtoken");
const gql = require("graphql-tag");
const apollo_client = require("./apollo");

const register = async (req, res) => {
  const { phone_no } = req.body.input;
  try {
    let findUser = await apollo_client.query({
      query: gql`
        query ($phone_no: String!) {
          user(where: { phone_no: { _eq: $phone_no } }) {
            name
            has_completed
            id
            phone_no
          }
        }
      `,
      variables: {
        phone_no,
      },
    });

    if (!findUser.data.user.length) {
      return res.send({
        message: "You are not allowed to join us",
      });
    }

    console.log("find user: ", findUser.data);

    const token_content = {
      sub: `${findUser.data.user[0].id}`,
      iat: Date.now() / 1000,
      "https://hasura.io/jwt/claims": {
        "x-hasura-allowed-roles": ["admin", "user"],
        "x-hasura-user-id": `${findUser.data.user[0].id}`,
        "x-hasura-phone_no": findUser.data.user[0].phone_no,
        "x-hasura-name": findUser.data.user[0].name,
        "x-hasura-role": "user",
        "x-hasura-default-role": "user",
        has_completed: findUser.data.user[0].has_completed,
      },
    };

    const token = jwt.sign(token_content, process.env.JWT_SECRET, {
      expiresIn: process.env.JWT_EXPIRES_IN,
    });

    return res.json({
      token,
    });
  } catch (err) {
    return res.status(400).json({
      message: err.message,
    });
  }
};

module.exports = register;
