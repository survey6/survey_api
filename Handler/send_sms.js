const apollo_client = require("./apollo");
const gql = require("graphql-tag");

const AfricasTalking = require("africastalking");

const africastalking = AfricasTalking({
  apiKey: process.env.AFRICASTALKING_API_KEY,
  username: "hahu",
});

const sendSms = async (req, res) => {
  let cardNo = "";
  let text = "";

  try {
    let data = await apollo_client.query({
      query: gql`
        query {
          gift(where: { taken: { _eq: false } }, limit: 1) {
            card_no
            id
            taken
          }
        }
      `,
    });

    if (data.data.gift.length) {
      cardNo = data.data.gift[0].card_no;
    }

    console.log("sms: ", data);

    if (data.data.gift.length === 0) {
      text = `ውድ ${req.body.input.user_name}: መጠይቁን ስለሞሉ እናመሰግናለን።`;
    } else
      text = `ውድ ${req.body.input.user_name}: መጠይቁን ስለሞሉ እናመሰግናለን። የነፃ የአየር ሰዓት ቁጥርዎ ፦ ${data.data.gift[0].card_no}`;

    await africastalking.SMS.send({
      to: req.body.input.phone_no,
      message: text,
      from: "hahujobs",
    });

    if (data.data.gift.length > 0) {
      await apollo_client.mutate({
        mutation: gql`
          mutation ($id: Int!, $userId: Int!) {
            update_gift_by_pk(
              pk_columns: { id: $id }
              _set: { taken: true, user_id: $userId }
            ) {
              id
            }
          }
        `,
        variables: {
          id: data.data.gift[0].id,
          userId: req.body.input.user_id,
        },
      });
    }

    res.send({
      success: true,
      via: text,
      card: cardNo,
    });
  } catch (error) {
    console.error(error);
    res.send(error);
  }
};

module.exports = sendSms;
