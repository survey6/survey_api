SET check_function_bodies = false;
CREATE TABLE public.answer (
    id integer NOT NULL,
    question_id integer NOT NULL,
    choice_id integer,
    text_answer text,
    user_id text NOT NULL
);
CREATE SEQUENCE public.answer_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.answer_id_seq OWNED BY public.answer.id;
CREATE TABLE public.choice (
    id integer NOT NULL,
    choice_text text NOT NULL,
    question_id integer NOT NULL,
    name text
);
CREATE SEQUENCE public.choice_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.choice_id_seq OWNED BY public.choice.id;
CREATE TABLE public.gift (
    id integer NOT NULL,
    card_no text NOT NULL,
    user_id text NOT NULL
);
CREATE SEQUENCE public.gift_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.gift_id_seq OWNED BY public.gift.id;
CREATE TABLE public.question (
    id integer NOT NULL,
    question text NOT NULL,
    type text NOT NULL
);
CREATE SEQUENCE public.question_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.question_id_seq OWNED BY public.question.id;
CREATE TABLE public."user" (
    phone_no text NOT NULL,
    name text NOT NULL,
    id text NOT NULL,
    has_completed boolean DEFAULT false NOT NULL
);
ALTER TABLE ONLY public.answer ALTER COLUMN id SET DEFAULT nextval('public.answer_id_seq'::regclass);
ALTER TABLE ONLY public.gift ALTER COLUMN id SET DEFAULT nextval('public.gift_id_seq'::regclass);
ALTER TABLE ONLY public.answer
    ADD CONSTRAINT answer_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.choice
    ADD CONSTRAINT choice_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.gift
    ADD CONSTRAINT gift_card_no_key UNIQUE (card_no);
ALTER TABLE ONLY public.gift
    ADD CONSTRAINT gift_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.gift
    ADD CONSTRAINT gift_user_id_key UNIQUE (user_id);
ALTER TABLE ONLY public.question
    ADD CONSTRAINT question_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_id_key UNIQUE (id);
ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_phone_no_key UNIQUE (phone_no);
ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.answer
    ADD CONSTRAINT answer_choice_id_fkey FOREIGN KEY (choice_id) REFERENCES public.choice(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.answer
    ADD CONSTRAINT answer_question_id_fkey FOREIGN KEY (question_id) REFERENCES public.question(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.answer
    ADD CONSTRAINT answer_user_id_fkey FOREIGN KEY (user_id) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.choice
    ADD CONSTRAINT choice_question_id_fkey FOREIGN KEY (question_id) REFERENCES public.question(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.gift
    ADD CONSTRAINT gift_user_id_fkey FOREIGN KEY (user_id) REFERENCES public."user"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
