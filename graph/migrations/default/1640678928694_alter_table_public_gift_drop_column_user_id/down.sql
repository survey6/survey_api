alter table "public"."gift" add constraint "gift_user_id_key" unique (user_id);
alter table "public"."gift" alter column "user_id" drop not null;
alter table "public"."gift" add column "user_id" text;
