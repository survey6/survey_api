alter table "public"."user" add constraint "user_id_key" unique (id);
alter table "public"."user" alter column "id" drop not null;
alter table "public"."user" add column "id" uuid;
