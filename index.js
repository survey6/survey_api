const express = require("express");
const cors = require("cors");
require("dotenv").config({ path: "./.env" });

const register_handler = require("./Handler/register");
const send_sms = require("./Handler/send_sms");

const app = express();

const port = process.env.EXPRESS_PORT || 8000;

app.use(cors());
app.use(express.json({ limit: "10MB" }));

app.post("/register", register_handler);
app.post("/sms", send_sms);

app.listen(port, () => {
  console.log(`App is listening on port  ${port}`);
});
