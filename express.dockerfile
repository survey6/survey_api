FROM node:lts-alpine3.9

WORKDIR /usr/src/app    

ARG EXPRESS_PORT

EXPOSE ${EXPRESS_PORT}

RUN apk add npm

COPY package.json package-lock.json ./

RUN npm install

COPY . .

CMD ["node", "index.js"]